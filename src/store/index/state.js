export const state = {
  showRegisterForm: false,
  showLoginForm: false,
  showResetPasswordForm: false,
  showCreateMurr: false,
  murrContent: false,
  murrCards: [],
  nextMurrCardsPage: null,
  murrCardsCount: 0
};
