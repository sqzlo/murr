from django.contrib import admin

from murren.models import Murren, TrustedRegistrationEmail


admin.site.register(Murren)
admin.site.register(TrustedRegistrationEmail)
