from allauth.socialaccount.providers.google.views import GoogleOAuth2Adapter
from allauth.socialaccount.providers.vk.views import VKOAuth2Adapter
from allauth.socialaccount.providers.oauth2.client import OAuth2Client
from django.contrib.auth import get_user_model
from django.core.files.images import ImageFile
from django.db.models import Count
from django.http import HttpResponse
from rest_auth.registration.views import SocialLoginView
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from murren.serializers import MurrenSerializer, VKOAuth2Serializer
from .permissions import IsAuthenticatedAndMurrenOrReadOnly, IsNotProfileOwner
from .services import make_resize_image, make_crop_image

Murren = get_user_model()


class MurrenPagination(PageNumberPagination):
    page_size = 30
    page_size_query_param = 'murren_len'
    max_page_size = 60


class MurrenViewSet(ModelViewSet):
    serializer_class = MurrenSerializer
    permission_classes = [IsAuthenticatedAndMurrenOrReadOnly]

    def get_queryset(self):
        queryset = Murren.objects.filter(is_active=True)\
            .annotate(subscribers_count=Count('subscribers'))\
            .order_by('-date_joined')
        return queryset

    @action(detail=False, permission_classes=[IsAuthenticated])
    def profile(self, request):
        serializer = self.get_serializer(instance=request.user)
        return Response(serializer.data)

    @action(detail=False, permission_classes=[IsAuthenticated], methods=['POST'])
    def avatar_resize(self, request):
        file_avatar = request.FILES.get('avatar')
        b_image, error = make_resize_image(file_avatar, (500, 500))
        if error:
            return Response({"error": error}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return HttpResponse(b_image, content_type='image/jpeg')

    @action(detail=False, permission_classes=[IsAuthenticated], methods=['PATCH'])
    def avatar_update(self, request):
        file_avatar = request.FILES.get('avatar')
        crop_x = int(request.data.get('crop.x'))
        crop_y = int(request.data.get('crop.y'))
        crop_width = int(request.data.get('crop.width'))
        crop_height = int(request.data.get('crop.height'))

        b_image, error = make_crop_image(
            file_avatar, (crop_x, crop_y, crop_width, crop_height))

        if error:
            return Response({"error": error}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        murren_avatar = ImageFile(b_image, name=f"{request.user.pk}.jpeg")
        if request.user.murren_avatar != 'default_murren_avatar.png':
            request.user.murren_avatar.delete()
        request.user.murren_avatar = murren_avatar
        request.user.save()

        serializer = self.get_serializer(instance=request.user)

        return Response(serializer.data)

    @action(detail=True, permission_classes=[IsAuthenticated&IsNotProfileOwner], methods=['POST'])
    def subscribe(self, request, pk=None):
        instance = self.get_object()
        subscriptions = request.user.subscriptions
        is_subscriber = subscriptions.filter(id=instance.id)

        if not is_subscriber:
            subscriptions.add(instance)
        else:
            subscriptions.remove(instance)

        return Response({
            'is_subscriber': instance.in_subscribers(request.user),
            'subscribers_count': instance.subscribers.count()
        })

    @action(detail=False, permission_classes=[IsAuthenticated])
    def subscriptions(self, request):
        subscriptions = request.user.subscriptions\
            .annotate(subscribers_count=Count('subscribers'))\
            .order_by('subscribers_count')
        subscriptions = self.paginate_queryset(subscriptions)

        serializer = self.get_serializer(instance=subscriptions, many=True)

        return self.get_paginated_response(serializer.data)


class GoogleLogin(SocialLoginView):
    adapter_class = GoogleOAuth2Adapter


class VkLogin(SocialLoginView):
    adapter_class = VKOAuth2Adapter
    serializer_class = VKOAuth2Serializer
    client_class = OAuth2Client
