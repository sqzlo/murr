from django.urls import path

from murr_websocket.consumers.murr_battle_room import MurrBattleRoomConsumer
from murr_websocket.consumers.murr_chat import MurrChatConsumer


websocket_urls = [
    path('ws/murr_chat/<str:murr_ws_name>/', MurrChatConsumer),
    path('ws/murr_battle_room/<str:murr_ws_name>/', MurrBattleRoomConsumer),
]
