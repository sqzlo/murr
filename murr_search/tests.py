import pytest
from django.urls import reverse
from rest_framework import status


pytestmark = [
    pytest.mark.django_db,
]


@pytest.mark.parametrize('count', [3])
def test_murr_card_search(create_release_murr_list, api_client):
    murr_card_list = create_release_murr_list['murr_card_list']

    expected_index = 2
    url = f'{reverse("search_murr_card")}?search={expected_index}'
    response = api_client.get(url)
    resp_json = response.json()
    result_object = resp_json['results'][0]
    expected_murr_card = murr_card_list[expected_index]

    assert response.status_code == status.HTTP_200_OK
    assert resp_json['count'] == 1
    assert result_object['id'] == expected_murr_card.id
    assert result_object['title'] == expected_murr_card.title
    assert result_object['content'] == expected_murr_card.content


@pytest.mark.parametrize('count', [3])
def test_murren_search(create_murren_list, api_client):
    murren_list = create_murren_list

    expected_index = 1
    url = f'{reverse("search_murren")}?search={expected_index}'
    response = api_client.get(url)
    resp_json = response.json()
    result_object = resp_json['results'][0]
    expected_murren = murren_list[expected_index]
    assert response.status_code == status.HTTP_200_OK
    assert resp_json['count'] == 1
    assert result_object['id'] == expected_murren.id


@pytest.mark.parametrize('count', [3])
def test_murr_card_wrong_search(create_release_murr_list, api_client):
    url = f'{reverse("search_murr_card")}?search=ABRAKADABRA'
    response = api_client.get(url)
    assert response.status_code == status.HTTP_200_OK
    assert response.json()['count'] == 0


@pytest.mark.parametrize('count', [3])
def test_murren_wrong_search(create_murren_list, api_client):
    url = f'{reverse("search_murren")}?search=ABRAKADABRA'
    response = api_client.get(url)
    assert response.status_code == status.HTTP_200_OK
    assert response.json()['count'] == 0
