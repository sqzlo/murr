from django.db.models import Count
from django.contrib.auth import get_user_model
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.decorators import action
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from murr_back.settings import BACKEND_URL
from .models import MurrCard, MurrCardStatus, Category
from murr_card.permissions import IsAuthenticatedAndOwnerOrReadOnly
from murr_card.serializers import (
    EditorImageForMurrCardSerializers,
    MurrCardSerializers,
    MurrCardCategoriesSerializer,
)
from murr_card.services import generate_user_cover
from murr_rating.services import RatingActionsMixin

Murren = get_user_model()


class MurrPagination(PageNumberPagination):
    page_size = 30
    page_size_query_param = 'murr_card_len'
    max_page_size = 60


class MurrCardViewSet(RatingActionsMixin, ModelViewSet):
    serializer_class = MurrCardSerializers
    permission_classes = [IsAuthenticatedAndOwnerOrReadOnly]
    pagination_class = MurrPagination
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['owner', 'category__slug']

    def get_queryset(self):
        queryset = MurrCard.objects.select_related('owner') \
            .annotate(likes=Count('liked_murrens', distinct=True),
                      dislikes=Count('disliked_murrens', distinct=True),
                      comments_count=Count('comments', distinct=True)) \
            .filter(status=MurrCardStatus.RELEASE) \
            .order_by('-timestamp')

        return queryset

    def create(self, request, *args, **kwargs):
        request.data['owner'] = request.user.id
        request.data['cover'] = generate_user_cover(request.data.get('cover'))
        request.data['status'] = request.data.get('status', MurrCardStatus.DRAFT.value)
        category = Category.objects.filter(slug=request.data.pop('category', None)).first()
        if category is not None:
            request.data['category'] = category.slug
        serializer = MurrCardSerializers(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=201, headers=headers)

    @action(detail=False)
    def category_list(self, request):
        queryset = Category.objects.all()
        queryset = self.paginate_queryset(queryset)
        serializer = MurrCardCategoriesSerializer(queryset, many=True)
        return self.get_paginated_response(serializer.data)

    @action(detail=False, permission_classes=[IsAuthenticated])
    def my_likes(self, request):
        queryset = self.get_queryset().filter(liked_murrens=request.user)
        return self.paginate_murr_card(queryset)

    @action(detail=False, permission_classes=[IsAuthenticated])
    def feed(self, request):
        subscriptions = Murren.objects.filter(subscribers=request.user)
        queryset = self.get_queryset().filter(owner__in=subscriptions)
        return self.paginate_murr_card(queryset)
    
    def paginate_murr_card(self, queryset):
        cards_on_page = self.paginate_queryset(queryset)
        serializer = self.get_serializer(cards_on_page, many=True)
        return self.get_paginated_response(serializer.data)


class EditorImageForMurrCardView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        serializer = EditorImageForMurrCardSerializers(data=request.data)
        murr_dict = {"success": 0, "file": {"url": ""}}
        if serializer.is_valid():
            serializer.save()
            url = BACKEND_URL + serializer.data['murr_editor_image']
            murr_dict = {"success": 1, "file": {"url": url}}
        return Response(murr_dict)
